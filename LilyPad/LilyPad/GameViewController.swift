//
//  GameViewController.swift
//  LilyPad
//
//  Created by Lianpeng Zhang on 16/3/3.
//  Copyright (c) 2016年 cs441. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    /*
    set up the initial scene of your program
    */
    override func viewWillLayoutSubviews() {
        // Configure the view.
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        let scene = GameMenu(size: skView.frame.size)
        
        /* Set the scale mode to scale to fit the window */
        scene.scaleMode = .AspectFill
        
        skView.presentScene(scene)
    }
    
    /*
    auto rotation setting. return true if your program needs to be rotated automatically.
    return a boolean.
    */
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    
    /*
    return type at the end of the function signature.
    */
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
