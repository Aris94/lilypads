//
//  LilyPad.swift
//  LilyPad
//
//  Created by Aris Agdere on 3/9/16.
//  Copyright © 2016 cs441. All rights reserved.
//

import Foundation
import SpriteKit

class LilyPad: SKSpriteNode {
    var letter:String = ""
    
    init(){
        super.init(texture: SKTexture(), color: UIColor(), size: CGSize())
    }
    
    init(tex:SKTexture, color:UIColor, texSize:CGSize, charFrequencyArray:[Character]){
        let count = charFrequencyArray.count
        let randIndex = arc4random_uniform(UInt32(count))
        letter = String(charFrequencyArray[Int(randIndex)])
        super.init(texture: tex, color: color, size: texSize)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeColor(colorIn:UIColor){
        self.color = colorIn;
    }
    
    func changeLetter(charFrequencyArray:[Character]){
        let count = charFrequencyArray.count
        let randIndex = arc4random_uniform(UInt32(count))
        letter = String(charFrequencyArray[Int(randIndex)])
    }
}