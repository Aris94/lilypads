//
//  GameMenu.swift
//  LilyPad
//
//  Created by Lianpeng Zhang on 16/3/3.
//  Copyright © 2016年 cs441. All rights reserved.
//

import Foundation
import SpriteKit


/*
set up the scene.
*/
class GameMenu: SKScene {
    
    var isPhone:Bool = true
    var screenWidth:CGFloat = 0
    var screenHeight:CGFloat = 0
    let gameTitle:SKLabelNode = SKLabelNode(fontNamed: "Star Jedi Rounded")
    let instructionLabel:SKLabelNode = SKLabelNode(fontNamed: "BM germar")
    var introImage:SKSpriteNode?
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            isPhone = false
        }
        else{
            isPhone = true
        }
        
        screenWidth = UIScreen.mainScreen().bounds.width
        screenHeight = UIScreen.mainScreen().bounds.height
        
        //print(screenWidth)
        //print(screenHeight)
        
        /*
        what does the anchor point do exactly?
        */
        
        self.backgroundColor = SKColor.blackColor()
        self.anchorPoint = CGPointMake(0.5,0)
        
        
        let tex:SKTexture = SKTexture(imageNamed: "memuBackground")
        let texSize:CGSize = CGSize(width: screenWidth,height: screenHeight)
        introImage = SKSpriteNode(texture: tex, color: UIColor.whiteColor(), size: texSize)
        addChild(introImage!)
        introImage!.position = CGPointMake(0,screenHeight/2)
        createInstructionLabel()
    }
    
    func createInstructionLabel(){
        gameTitle.text = "Lily Pads"
        gameTitle.horizontalAlignmentMode = .Center
        gameTitle.verticalAlignmentMode = .Center
        gameTitle.fontColor = SKColor.blackColor()
        gameTitle .zPosition = 1
        addChild(gameTitle)
        gameTitle.fontSize = 70
        
        
        
        instructionLabel.text = "Touch To Begin Game"
        instructionLabel.horizontalAlignmentMode = .Center
        instructionLabel.verticalAlignmentMode = .Center
        instructionLabel.fontColor = SKColor.redColor()
        instructionLabel.zPosition = 2
        addChild(instructionLabel)
        
        if(isPhone == true)
        {
            instructionLabel.position = CGPointMake(0,screenHeight * 0.15)
            gameTitle.position = CGPointMake(0,screenHeight * 0.7)
            instructionLabel.fontSize = 30
            gameTitle.fontSize = 60
        }
        else{
            instructionLabel.position = CGPointMake(0,screenHeight * 0.25)
            gameTitle.position = CGPointMake(0,screenHeight * 0.8)
            instructionLabel.fontSize = 40
            gameTitle.fontSize = 70
        }
        
        // Create a blink instruction label using SKAction.
        // Create a squence and run it using SKAction.
        let wait:SKAction = SKAction.waitForDuration(1)
        let fadeDown:SKAction = SKAction.fadeAlphaTo(0, duration: 0.2)
        let fadeup:SKAction = SKAction.fadeAlphaTo(1, duration: 0.2)
        let seq:SKAction = SKAction.sequence([wait,fadeDown,fadeup])
        let repeatSeq:SKAction = SKAction.repeatActionForever(seq)
        instructionLabel.runAction(repeatSeq)
        
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        // Transition to the wgame scene.
        let transition:SKTransition = SKTransition.revealWithDirection(SKTransitionDirection.Down, duration: 1.0)
        
        let scene = GameScene(size: self.scene!.size)
        
        scene.scaleMode = SKSceneScaleMode.AspectFill
        
        self.scene!.view!.presentScene(scene, transition: transition)
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}