//
//  GameScene.swift
//  LilyPad
//
//  Created by Lianpeng Zhang on 16/3/3.
//  Copyright (c) 2016年 cs441. All rights reserved.
//

import SpriteKit
import AVFoundation


struct PhysicsCategory {
    static let leftLily : UInt32 = 0b1
    static let leftWall: UInt32 = 0b11
    static let rightLily: UInt32 = 0b100
    static let rightWall: UInt32 = 0b1100
    static let frog: UInt32 = 0b1111
}

class GameScene: SKScene, SKPhysicsContactDelegate{
    var isPhone:Bool = true
    var screenWidth:CGFloat = 0
    var screenHeight:CGFloat = 0
    var river:SKSpriteNode?
    var river2:SKSpriteNode?
    var topBank:SKSpriteNode?
    var downBank:SKSpriteNode?
    var lilyCount = 0;
    let rows:Int = 4; //rows of lillies
    let lpr:Int = 8; //lillies per row
    var totalLillies:Int = 0;
    var lilyWidth:Double = 0.0;
    var lilyHeight:Double = 0.0;
    var frog:SKSpriteNode = SKSpriteNode();
    var firstMovement:Bool = true;
    let lilySpeed:CGFloat = 50.0;
    var mainView:UIView = UIView();
    var lilyUnderFrog:LilyPad = LilyPad();
    var leftWall:SKSpriteNode = SKSpriteNode();
    var rightWall:SKSpriteNode = SKSpriteNode();
    var ignoreOtherLilyCollisions = false;
    var frogRespawnX = 0.0;
    var magnifier:Double = 0.0
    let frogWrap:Bool = false
    var highScore:Int = 0
    var currentScore:Int = 0
    var wordArray:[String] = [String]()
    var wordProgressLabel:SKLabelNode = SKLabelNode()
    var wordGoalLabel:SKLabelNode = SKLabelNode()
    var livesLeft:Int = 0
    var lives:Int = 3
    var livesLabel:SKLabelNode = SKLabelNode()
    var currentScoreLabel:SKLabelNode = SKLabelNode()
    var highScoreLabel:SKLabelNode = SKLabelNode()
    var wordGoalLength:Int = 0
    var wordProgressLength:Int = 0
    var currentLetter:String = " "
    var time:Int = 10
    var timeLeft:Int = 0
    var timer:NSTimer = NSTimer()
    var letterTimer:NSTimer = NSTimer()
    var timerLabel:SKLabelNode = SKLabelNode()
    var category:String = "English Words"
    var fileName:String = "words.txt"
    var lettersFileName:String = "wordsLetters.txt"
    var charArray:[[Character]] = [[Character]]()
    let letterChangeTime = 5.0
    var wordIndex:Int = 0
    var charFrequencyDictionary = [Character:Int]()
    var totalLetters = 0
    var frogOnLily = false
    var maxLength = 17
    var giveUpButton:SKSpriteNode = SKSpriteNode()
    var bgSoundPlayer:AVAudioPlayer?
    var numberMode:Bool = false
    //var wordLists:
    /*
        Function will be called automatically 
        when the game scene is loaded.
    */
    override func didMoveToView(view: SKView) {
        
        self.physicsWorld.gravity = CGVectorMake(0,0)
        self.physicsWorld.contactDelegate = self
        
        /*
            check device type
        */
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            isPhone = false
        }
        else{
            isPhone = true
        }
        
        
        //game set up
        assignGlobalVariables();
        setupWalls();
        setupBackground();
        spawnLillies();
        spawnFrog(0.0, y: 0.0, firstFrog: true);
        
        setupLabel(wordProgressLabel, labelPosition: CGPoint(x: screenWidth * 0.2, y: screenHeight * 0.95), color: UIColor.redColor());
        setupLabel(wordGoalLabel, labelPosition: CGPoint(x: screenWidth * 0.8, y: screenHeight * 0.95),color: UIColor.redColor())
        setupLabel(livesLabel, labelPosition: CGPoint(x: screenWidth * 0.8, y: screenHeight * 0.06),color: UIColor.redColor())
        setupLabel(timerLabel, labelPosition: CGPoint(x: screenWidth * 0.8, y: screenHeight * 0.14),color: UIColor.redColor())
        setupLabel(highScoreLabel, labelPosition: CGPoint(x: screenWidth * 0.2, y: screenHeight * 0.07), color: UIColor.redColor())
        setupLabel(currentScoreLabel, labelPosition: CGPoint(x: screenWidth * 0.2, y: screenHeight * 0.14), color: UIColor.redColor())
        //setupButton(giveUpButton)
        giveUpButton.name = "give up"
        
        if(category=="All Words"){
            wordGoalLabel.text = selectRandomWord()
        }else{
            wordGoalLabel.text = category
        }
        wordGoalLength = (wordGoalLabel.text?.characters.count)!
        livesLabel.text = "Lives: " + String(lives)
        timerLabel.text = String(time)
        currentScoreLabel.text = "Score: " + String(currentScore)
        highScoreLabel.text = "Highscore: " + String(highScore)
        startTimer()
        playBackgroundSound("bg")
    }
    
    func setupButton(var button:SKSpriteNode){
        let texture = SKTexture(imageNamed: "retry")
        let color = UIColor.blackColor()
        let size = CGSize(width: screenWidth * 0.1, height: screenHeight * 0.1)
        button.position = CGPoint(x: screenWidth * 0.8, y: screenHeight * 0.95)
        button = SKSpriteNode(texture: texture, color: color, size: size)
        button.zPosition = 102
        self.addChild(button)
    }
    
    func populateFrequencyDictionary(wordArray:[String]) -> [Character:Int]{
        var charDict:[Character:Int] = [Character:Int]()
        for word in wordArray{
            for char in word.uppercaseString.characters{
                totalLetters++
                if(charDict[char] != nil){
                    charDict[char] = charDict[char]! + 1
                }else{
                    charDict[char] = 1
                }
            }
        }
        return charDict
    }
    
//    func populateLetterFrequencyByIndex(wordArray:[String], letterIndex:Int) -> [Character]{
//        var letterArray = [Character]()
//        for i in 0...wordArray.count-1{
//            if(wordArray[i].characters.count-1 >= letterIndex){
//                print(wordArray[i].characters.prefix(letterIndex+1))
//                letterArray.append(wordArray[i].uppercaseString.characters.prefix(letterIndex+1).last!)
//            }
//        }
//        return letterArray
//    }

    
    func fileToArrayOfLines(fileName:String) -> Array<String>{
        //returns array of file lines or empty array if failed
        var readString: String
        let filePath = NSBundle.mainBundle().pathForResource(fileName, ofType: nil)
        
        do {
            readString = try NSString(contentsOfFile: filePath!, encoding: NSUTF8StringEncoding) as String
            //Loop to add each word
            return readString.componentsSeparatedByString("\n")
        } catch let error as NSError {
            print(error.description)
        }
        return Array<String>()
    }
    
    func startTimer(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)
        letterTimer = NSTimer.scheduledTimerWithTimeInterval(letterChangeTime, target: self, selector: Selector("updateLetter"), userInfo: nil, repeats: true)
    }

    func updateTimer(){
        if(frogOnLily){
            timeLeft++
            timerLabel.text = String(timeLeft)
        }else{
            if(timeLeft>1){
                timeLeft--
                timerLabel.text = String(timeLeft)
            }else{
                playSound("buzzer")
                respawn(false)
                timerLabel.text = String(timeLeft)
            }
        }
    }
    
    func updateLetter() {
        self.enumerateChildNodesWithName("*"){
            node, stop in
            if let temp = node as? LilyPad {
                var index = 0
                if(self.wordProgressLength != 0){
                    index = self.wordProgressLength-1
                }
                temp.changeLetter(self.charArray[index])
                temp.enumerateChildNodesWithName("Letter"){
                    node, stop in
                    if let thisLabel = node as? SKLabelNode{
                        thisLabel.text = String(temp.letter)
                        thisLabel.fontColor = self.getRandomColor()
                    }

                }
                //stop.memory = true
            }
        }
    }
    
    func switchLists(newFileName:String,newLettersFileName:String){
        wordArray = fileToArrayOfLines(newFileName)
        charArray = createCharArray(newLettersFileName)
        charFrequencyDictionary = self.populateFrequencyDictionary(wordArray)
    }
    
    func setupLabel(label:SKLabelNode, labelPosition:CGPoint, color:UIColor){
        let convertedLabelPosition = convertPointFromView(labelPosition)
        label.text = ""
        label.name = "Label"
        label.fontColor = color
        label.fontName = "Discoteca Rounded"
        label.position = convertedLabelPosition
        label.zPosition = 3
        self.addChild(label)
    }
    
    func calculateWordValue(word:String) -> Int{
        let uppercase = word.uppercaseString
        var score:Double = pow(3.0, Double(word.characters.count))
        for char in uppercase.characters{
            let numerator = Double(charFrequencyDictionary[char]!)
            let denomonator = Double(totalLetters)
            let freq = numerator / denomonator * 100.0
            score += round(100.0 * Double(word.characters.count)/freq)
        }
        return Int(score)
    }
    
    func assignGlobalVariables(){
        screenWidth = UIScreen.mainScreen().bounds.width
        screenHeight = UIScreen.mainScreen().bounds.height
        mainView = self.view!
        magnifier = 6/Double(rows)
        lilyHeight = Double(screenHeight * 0.09) * magnifier;
        lilyWidth = Double(screenHeight * 0.09) * magnifier;
        
        wordArray = fileToArrayOfLines(fileName)
        charArray = createCharArray(lettersFileName)
        charFrequencyDictionary = self.populateFrequencyDictionary(wordArray)
        
        timeLeft = time
        livesLeft = lives
    }
    
    func setupBackground(){
        self.backgroundColor = SKColor.greenColor()
        self.anchorPoint = CGPointMake(0.5,0)
        let tex:SKTexture = SKTexture(imageNamed: "river")
        let texSize:CGSize = CGSize(width: screenWidth,height: 0.7 * screenHeight)
        river = SKSpriteNode(texture: tex, color: UIColor.whiteColor(), size: texSize)
        river?.name = "river"
        self.addChild(river!)
        river!.position = CGPointMake(0,screenHeight/2)
        river!.zPosition = 2
        river2 = SKSpriteNode(texture: tex, color: UIColor.whiteColor(), size: texSize)
        river2?.name = "river"
        self.addChild(river2!)
        river2!.position = CGPointMake(screenWidth,screenHeight/2)
        
        let topTex:SKTexture = SKTexture(imageNamed: "topBank")
        let topTextSize:CGSize = CGSize(width: screenWidth,height: 0.15 * screenHeight)
        topBank = SKSpriteNode(texture: topTex, color:UIColor.clearColor(), size: topTextSize)
        topBank?.name = "topRiverBank"
        topBank!.zPosition = 1
        self.addChild(topBank!)
        topBank!.position = CGPointMake(0,0.925*screenHeight)
        
        
        let downTex:SKTexture = SKTexture(imageNamed: "botBank")
        let downTextSize:CGSize = CGSize(width: screenWidth,height: 0.15 * screenHeight)
        downBank = SKSpriteNode(texture: downTex, color:UIColor.clearColor(), size: downTextSize)
        downBank?.name = "downRiverBank"
        downBank!.zPosition = 1
        self.addChild(downBank!)
        downBank!.position = CGPointMake(0,0.075*screenHeight)
        
        startLoopingBackground()
    }
    
    func startLoopingBackground(){
        let move:SKAction = SKAction.moveByX(-screenWidth, y: 0, duration: 40)
        let moveBack:SKAction = SKAction.moveByX(screenWidth, y: 0, duration: 0)
        let seq:SKAction = SKAction.sequence([move,moveBack])
        let rep:SKAction = SKAction.repeatActionForever(seq)
        river!.runAction(rep)
        river2!.runAction(rep)
    }
    
    func setupWalls(){
        leftWall = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: 5, height: self.size.height))
        rightWall = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: 5, height: self.size.height))
        leftWall.zPosition = 99;
        rightWall.zPosition = 99;
        leftWall.position = CGPoint(x: CGFloat(Double(-screenWidth/2) - lilyWidth), y: screenHeight/2)
        rightWall.position = CGPoint(x: CGFloat(Double(screenWidth/2) + lilyWidth), y: screenHeight/2)
        
        leftWall.physicsBody = SKPhysicsBody(rectangleOfSize: leftWall.size)
        leftWall.physicsBody!.categoryBitMask = PhysicsCategory.leftWall;
        leftWall.physicsBody!.contactTestBitMask = PhysicsCategory.leftLily;
        leftWall.physicsBody!.collisionBitMask = 0;
        leftWall.physicsBody?.affectedByGravity = false;
        leftWall.physicsBody!.dynamic = true;
        leftWall.name = "leftwall"
        
        rightWall.physicsBody = SKPhysicsBody(rectangleOfSize: rightWall.size)
        rightWall.physicsBody!.categoryBitMask = PhysicsCategory.rightWall;
        rightWall.physicsBody!.contactTestBitMask = PhysicsCategory.rightLily;
        rightWall.physicsBody!.collisionBitMask = 0;
        rightWall.physicsBody?.affectedByGravity = false;
        rightWall.physicsBody!.dynamic = true;
        rightWall.name = "rightwall"
        
        self.addChild(leftWall);
        self.addChild(rightWall);
    }
    
    func spawnFrog(var x:Double, var y:Double, firstFrog:Bool) -> SKSpriteNode{
        if(x==0.0 && y==0.0){
            y = Double(self.size.height * 0.075);
            x = 0.0;
        }
        var point:CGPoint = CGPoint(x: x, y: y);
        if(firstFrog == false){
            let convertedX:CGFloat = convertPointFromView(point).x
            point = CGPoint(x: convertedX, y: CGFloat(y))
        }
        let tex:SKTexture = SKTexture(imageNamed: "frog");
        let texSize:CGSize = CGSize(width: lilyWidth, height: lilyHeight*0.8);
        frog = SKSpriteNode(texture: tex, color: UIColor.greenColor(), size: texSize);
        frog.name = "0";
        frog.position = point;
        frog.zPosition = 1000;
        frog.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: lilyWidth, height: lilyHeight))
        frog.physicsBody!.categoryBitMask = PhysicsCategory.frog;
        frog.physicsBody!.contactTestBitMask = PhysicsCategory.leftWall | PhysicsCategory.rightWall | PhysicsCategory.leftLily | PhysicsCategory.rightLily;
        frog.physicsBody!.collisionBitMask = 0;
        frog.physicsBody?.affectedByGravity = false;
        frog.physicsBody!.dynamic = true;
        self.addChild(frog);
        return frog
    }
    
    func selectRandomWord() -> String{
        let randIndex:Int = Int(arc4random_uniform(UInt32(wordArray.count)))
        return wordArray[randIndex].uppercaseString
    }
    
    func spawnLillies(){
        totalLillies = rows * lpr;
        var y = Double(self.size.height) * 0.2;
        for _ in 1...rows{
            spawnLilyRow(y, lilliesPerRow: lpr);
            y += Double(self.size.height) * Double(0.7 / Double(rows));
        }
    }
    
    func spawnLilyRow(y:Double, lilliesPerRow:Int){
        //var x = Double(self.size.width) * Double(0.5 / Double(lilliesPerRow));
        var x = lilyWidth/2;
        for _ in 1...lpr{
            spawnLily(x, y: y, count: lilyCount, wasOldLily: false);
            lilyCount++;
            x += Double(self.size.width) * Double(1.025 / Double(lpr));
        }
    }
    
    func lilyReachesEdge(lily:LilyPad){
        var point:CGPoint;
        let num:Int = Int(lily.name!)!;
        if(lily.position.x < (self.size.width / 2)){
            //hit left edge
            spawnLily(Double(self.size.width), y: Double(lily.position.y), count: num, wasOldLily: true);
        }else{
            // hit right edge
            point = CGPoint(x: 0.0, y: Double(lily.position.y))
            spawnLily(Double(point.x), y: Double(point.y), count: num, wasOldLily: true);
        }
        lily.removeFromParent();
    }
    
    func frogReachesEdge(){
        if(frogWrap==false){
            respawn(false)
            playSound("splash")
        }else{
            ignoreOtherLilyCollisions = false
            frog.removeFromParent()
            //Might not use this feature, not sure yet
            frog = spawnFrog(frogRespawnX, y: Double(frog.position.y), firstFrog: false)
        }
    }
    
    func spawnLily(x:Double, y:Double, count:Int, wasOldLily:Bool){
        var action:SKAction;
        var point:CGPoint;
        let width:Double = Double(self.size.width);
        let tex:SKTexture = SKTexture(imageNamed: "GreenLily");
        let texSize:CGSize = CGSize(width: lilyWidth, height: lilyHeight);
        var index:Int = 0
        
        if(wordProgressLength != 0){
            index = wordProgressLength-1
        }
        
        let lp = LilyPad(tex: tex, color: UIColor.whiteColor(), texSize: texSize, charFrequencyArray: charArray[index]);
        
        lp.physicsBody = SKPhysicsBody(rectangleOfSize: lp.size)
        lp.physicsBody?.affectedByGravity = false;
        lp.physicsBody!.dynamic = false;
        
        if((count/lpr)%2==0){
            //move left
            point = convertPointFromView(CGPoint(x: x-(width*0.1), y: y));
            action = SKAction.moveByX(-lilySpeed, y: 0.0, duration: 1.0)
            lp.physicsBody!.categoryBitMask = PhysicsCategory.leftLily;
            lp.physicsBody!.contactTestBitMask = PhysicsCategory.leftWall;
        }else{
            //move right
            point = convertPointFromView(CGPoint(x: x+(width*0.1), y: y));
            action = SKAction.moveByX(lilySpeed, y: 0.0, duration: 1.0)
            lp.physicsBody!.categoryBitMask = PhysicsCategory.rightLily;
            lp.physicsBody!.contactTestBitMask = PhysicsCategory.rightWall;
        }
        lp.physicsBody!.collisionBitMask = 0;
        lp.name = String(count);
        point = CGPoint(x: x, y: y);
        let pointConverted = convertPointFromView(point);
        if(wasOldLily == false){
            lp.position = pointConverted;
        }else{
            //lp.position = point;
            lp.position = CGPoint(x: pointConverted.x, y: point.y)
        }
        lp.zPosition = 102;
        let endlessAction = SKAction.repeatActionForever(action);
        lp.runAction(endlessAction);
        //Add text to lily
        let letterLabel = SKLabelNode(text: String(lp.letter))
        letterLabel.fontColor = getRandomColor()
        letterLabel.fontName = "Discoteca Rounded"
        letterLabel.zPosition = 103
        letterLabel.name = "Letter"
        letterLabel.position.y -= CGFloat(lilyHeight)/4
        lp.addChild(letterLabel)
        self.addChild(lp);
    }
    

    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        //let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: 0, blue: randomBlue, alpha: 1.0)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        ignoreOtherLilyCollisions = false;
        var splash:Bool = true;
        var newY:CGFloat = 0.0
        var newX:CGFloat = 0.0
        let touchY:CGFloat = (touches.first?.locationInView(mainView).y)!
        let touchX:CGFloat = (touches.first?.locationInView(mainView).x)!
        let frogY:CGFloat = convertPointFromView(frog.position).y
        let frogX:CGFloat = convertPointToView(frog.position).x
        let xDifRatio = abs(touchX-frogX) / screenWidth
        let yDifRatio = abs(touchY-frogY) / screenHeight
        
        if(yDifRatio > xDifRatio){
            //Frog Moves Up or Down
            if(touchY < frogY){
                //Frog Moves Up
                //let lowerBound:Bool = frogY > screenHeight * 0.15
                //if(true){
                newY = ((self.size.height) * (0.7 / CGFloat(rows)));
                //}
            }else{
                //Frog Moves Down
                //if(frogY < screenHeight * 0.85){
                newY = -((self.size.height) * (0.7 / CGFloat(rows)));
                //}
            }
            
        }else{
            //Frog Moves Left or Right
            if(touchX > frogX){
                newX = (self.size.width / CGFloat(lpr));
            }else{
                newX = -(self.size.width / CGFloat(lpr));
            }
        }
        
        frogOnLily = false
        
        frog.position.y += newY;
        frog.position.x += newX;
        
        
        // check frog out of edges
        if(frog.position.x > rightWall.position.x ||
            frog.position.x < leftWall.position.x){
                frogReachesEdge()
        }
        
        // check on pad or river
        frog.removeAllActions();
        let nodes:[SKNode] = nodesAtPoint(frog.position)
        for node in nodesAtPoint(frog.position){
            if(node.name! != "river" &&
                node.name! != "0" &&
                node.name! != "skull"){
                    splash = false
            }
        }
        if(nodes.contains(river!)==false){
            splash = false
            playSound("jump")
            
        }
        if(splash){
            respawn(false)
            playSound("splash")
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func outOfLives(){
        let alert = UIAlertController(title: "You Lose!", message: ":[", preferredStyle: UIAlertControllerStyle.Alert)
        let playAgainAction = UIAlertAction(title: "Play Again", style: UIAlertActionStyle.Default, handler: playAgain)
        let quitAction = UIAlertAction(title: "Quit", style: UIAlertActionStyle.Default, handler: quit)
        alert.addAction(playAgainAction)
        alert.addAction(quitAction)
        self.view?.window?.rootViewController?.presentViewController(alert, animated: true, completion:pauseClock)
        //randomize
//        var fileInfo:[String] = randomNewFile()
//        let newFileName:String = fileInfo[0]
//        let newLettersFileName:String = fileInfo[1]
//        category = fileInfo[2]
//        switchLists(newFileName, newLettersFileName: newLettersFileName)
    }
    
    func randomNewFile() -> [String]{
        var array:[String] = [String]()
        let animalCategory = "Animal"
        let animalFileName = "animals.txt"
        let animalLettersFileName = "animalsLetters.txt"
        let wordCategory = "English Words"
        let wordFileName = "words.txt"
        let wordLettersFileName = "wordsLetters.txt"
        if(arc4random_uniform(1)==0){
            array.append(wordFileName)
            array.append(wordLettersFileName)
            array.append(wordCategory)
        }else{
            array.append(animalFileName)
            array.append(animalLettersFileName)
            array.append(animalCategory)
        }
        return array
    }
    
    func playAgain(alert:UIAlertAction){
        livesLeft = lives
        livesLabel.text = "Lives: " + String(livesLeft)
        if(category == "All Words"){
            wordGoalLabel.text = selectRandomWord()
        }
        unpauseClock()
    }
    
    func quit(alert:UIAlertAction){
        // To Be Implemented
    }
    
    func respawn(withLife:Bool){
        //Might not use
        frogOnLily = false
        if(withLife==false){
            if(livesLeft<2){
                outOfLives()
            }
            livesLeft--
            currentScore = 0
            currentScoreLabel.text = "Score: " + String(currentScore)
        }
        livesLabel.text = "Lives: " + String(livesLeft)
        frog.removeFromParent()
        let tex:SKTexture = SKTexture(imageNamed: "rip")
        let texSize:CGSize = CGSize(width: lilyWidth, height: lilyHeight)
        let skull:SKSpriteNode = SKSpriteNode(texture: tex, color: UIColor.whiteColor(), size: texSize)
        skull.position = frog.position
        skull.zPosition = 101
        skull.name = "skull"
        //self.addChild(skull)
        spawnFrog(0.0, y: 0.0, firstFrog: true)
        timer.invalidate()
        letterTimer.invalidate()
        timerLabel.text = String(time)
        wordProgressLabel.text = ""
        wordProgressLength = 0
        timeLeft = time
        wordIndex = 0
        startTimer()
    }
    
    func completedWord(){
        //prompt user to cash in?
        currentScore += calculateWordValue(wordProgressLabel.text!)
        currentScoreLabel.text = "Score: " + String(currentScore)
        self.view?.paused = true
        let promptToCashIn = UIAlertController(
            title: "Word Completed!",
            message: "Continue?",
            preferredStyle: UIAlertControllerStyle.Alert)
        let submitWord = UIAlertAction(
            title: "Cash In",
            style: UIAlertActionStyle.Default,
            handler: cashIn)
        let continueWord = UIAlertAction(
            title: "Keep Playing",
            style: UIAlertActionStyle.Default,
            handler: unpause)
        promptToCashIn.addAction(submitWord)
        promptToCashIn.addAction(continueWord)
        self.view?.window?.rootViewController?.presentViewController(
            promptToCashIn,
            animated: true,
            completion: nil)
    }
    
    func unpause(alert:UIAlertAction){
        self.view?.paused = false
    }
    
    func cashIn(alert:UIAlertAction){
        playSound("chaching")
        unpause(alert)
        if(currentScore > highScore){
            highScore = currentScore
            highScoreLabel.text = "Highscore: " + String(highScore)
        }
//        currentScore = 0
//        currentScoreLabel.text = "Score: " + String(currentScore)
        respawn(true)
    }
    
    func updateWordIndex(word:String){
        for i in wordIndex...wordArray.count-1{
            if(word > wordArray[i]){
                wordIndex++
            }else if(wordArray[wordIndex].containsString(word) == false){
                wordIndex = -1
                break
            }else{
                break
            }
        }
        
    }
    
    func loadCharArray(lettersFileName:String, listIndex:Int) -> [Character]{
        let array = fileToArrayOfLines(lettersFileName)
        var foundIndex:Bool = false
        //maxLength = Int(array[0])!
        var charArray = [Character]()
        for char in array.suffixFrom(1){
            if(foundIndex==false){
                if(Int(char) == listIndex){
                    foundIndex = true
                }
            }else{
                if(Int(char)==nil){
                    charArray.append(Character(String(Character(char)).uppercaseString))
                }else{
                    return charArray
                }
            }
        }
        return charArray
    }
    
    func createCharArray(lettersFileName:String) -> [[Character]]{
        var newArray = [[Character]]()
        //var temp = fileToArrayOfLines(lettersFileName)
        //maxLength = Int(temp[0])!
        for i in 1...maxLength{
            newArray.append(loadCharArray(lettersFileName,listIndex: i))
        }
        return newArray
    }
    
    func wrongLetter(){
        playSound("buzzer")
        //charArray = populateLetterFrequencyByIndex(wordArray, letterIndex: 0)
    }
    
    func frogContactsLily(frog:SKSpriteNode, lily:LilyPad){
        if(ignoreOtherLilyCollisions){
            return
        }
        
        let moveToLily:SKAction = SKAction.moveTo(lily.position, duration: 0)
        frog.runAction(moveToLily)
        lilyUnderFrog = lily
        frogOnLily = true
        if(lilyUnderFrog.letter != ""){
            //Letter for word
            wordProgressLabel.text = wordProgressLabel.text! + String(lilyUnderFrog.letter)
            wordProgressLength++
            timeLeft += 2
            timerLabel.text = String(timeLeft)
            let currentWord = wordProgressLabel.text!.lowercaseString
            updateWordIndex(currentWord)
            if(wordIndex > -1){
                playSound("jump")
                if(wordArray[wordIndex]==currentWord){
                    completedWord()
                }else{
                    //charArray = populateLetterFrequencyByIndex(wordArray, letterIndex: wordProgressLength-1)
                }
            }else{
                respawn(false)
                wrongLetter()
            }
            /*
            let array = Array(arrayLiteral: wordGoalLabel.text?.characters.map { String($0) })
            currentLetter = array[0]![wordProgressLength-1]
            if(lilyUnderFrog.letter != currentLetter){
                respawn()
                if(wordProgressLength>0){
                    wordProgressLabel.text = wordProgressLabel.text?.substringToIndex(wordProgressLabel.text!.endIndex.advancedBy(-1))
                }
                wordProgressLength = 0
                timer.invalidate()
            }else{
                lilyUnderFrog.letter = ""
                lily.letter = ""
                (lily.childNodeWithName("Letter") as! SKLabelNode).text = ""
            }
            */
            lilyUnderFrog.letter = ""
            lily.letter = ""
            (lily.childNodeWithName("Letter") as! SKLabelNode).text = ""
        }
        
        var action:SKAction
        if(lily.physicsBody?.categoryBitMask == PhysicsCategory.leftLily){
            action = SKAction.moveByX(-lilySpeed, y: 0.0, duration: 1.0)
            frogRespawnX = Double(mainView.frame.width)
        }else{
            action = SKAction.moveByX(lilySpeed, y: 0.0, duration: 1.0)
            frogRespawnX = 0.0
        }
        let endlessAction = SKAction.repeatActionForever(action)
        frog.runAction(endlessAction)
        ignoreOtherLilyCollisions = true
    }
    
    func pauseClock(){
        timer.invalidate()
    }
    
    func unpauseClock(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)
    }
    
    
    func frogContact(frogBody: SKPhysicsBody, secondBody: SKPhysicsBody){
        //First is frog
        if((secondBody.categoryBitMask == PhysicsCategory.leftWall) || (
            secondBody.categoryBitMask == PhysicsCategory.rightWall)){
                frogReachesEdge();
        }else if((secondBody.categoryBitMask == PhysicsCategory.leftLily) || (
                 secondBody.categoryBitMask == PhysicsCategory.rightLily)){
                    frogContactsLily(frogBody.node as! SKSpriteNode, lily: secondBody.node as! LilyPad);
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody = contact.bodyA
        var secondBody: SKPhysicsBody = contact.bodyB
        if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
            //bigger bitmask is bodyA
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        if(firstBody.categoryBitMask != PhysicsCategory.frog){
            lilyReachesEdge(secondBody.node as! LilyPad)
        }else{
            frogContact(firstBody, secondBody: secondBody)
        }
    }
    
    func playBackgroundSound(name:String){
        
        if(bgSoundPlayer != nil){
        
            bgSoundPlayer!.stop()
            bgSoundPlayer = nil
        
        }
        
        let url = NSBundle.mainBundle().URLForResource(name, withExtension: "mp3")
        guard let newURL = url else {
            print("Could not find file: \(name)")
            return
        }
        do {
            bgSoundPlayer = try AVAudioPlayer(contentsOfURL: newURL)
            bgSoundPlayer!.volume = 0.5
            bgSoundPlayer!.numberOfLoops = -1
            bgSoundPlayer!.prepareToPlay()
            bgSoundPlayer!.play()
        } catch let error as NSError {
            print(error.description)
        }
        
    }
    
    func playSound(name:String){
        let theSound:SKAction = SKAction.playSoundFileNamed(name, waitForCompletion: false)
        self.runAction(theSound)
    }
}
